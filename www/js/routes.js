/**
 * Created by AndresFabian on 7/10/2016.
 */
angular.module('starter').config(['$stateProvider', '$urlRouterProvider'
  , function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
      })


      .state('app.list', {
        url: '/list',
        cache: false,
        views: {
          'menuContent': {
            templateProvider: ['$http', function ($http) {
              return $http.get('templates/list.html')
                .then(function (response) {
                  return response.data;
                });
            }],
            controller: 'ListController'
          }
        }
      })
      .state('app.new', {
        url: '/new',
        cache: false,
        views: {
          'menuContent': {
            templateProvider: ['$http', function ($http) {
              return $http.get('templates/new.html')
                .then(function (response) {
                  return response.data;
                });
            }],
            controller: 'NewController'
          }
        }


      })
      .state('app.detail', {
        url: '/detail',
        cache: false,
        params:{
          post_id:null
        },
        views: {
          'menuContent': {
            templateProvider: ['$http', function ($http) {
              return $http.get('templates/detail.html')
                .then(function (response) {
                  return response.data;
                });
            }],
            controller: 'DetailController'
          }
        }


      })
    ;
    $urlRouterProvider.otherwise("app/list");

  }]);


/**
 * Created by AndresFabian on 6/03/2016.
 */
