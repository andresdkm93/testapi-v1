
ResourceFactory.$inject = ['$resource', 'baseURL'];
function ResourceFactory($resource, baseURL) {
  this.getService = function (entityName, params) {
    if (params)
      return $resource(baseURL  + entityName , params,
        {
          'update': {method: 'PUT'}
        });
    else
      return $resource(baseURL + entityName , null,
        {
          'update': {method: 'PUT'}
        });
  };



};

angular.module('starter').service("resourceFactory", ResourceFactory);
