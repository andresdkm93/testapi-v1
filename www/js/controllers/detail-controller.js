/**
 * Created by AndresFabian on 7/10/2016.
 */
/**
 * Created by AndresFabian on 7/10/2016.
 */
angular.module('starter')
  .controller("DetailController", ["$scope", "resourceFactory", "$ionicHistory", "$stateParams",
    function ($scope, resourceFactory, $ionicHistory, $stateParams) {
      var id = $stateParams.post_id;
      $scope.post={};
      resourceFactory.getService("posts/" + id)
        .get(function (response) {
          $scope.post = response;
        });


      $scope.update = function () {
        resourceFactory.getService("posts/"+id)
          .update($scope.post, function (response) {
            $ionicHistory.goBack();
          });
      }
      $scope.delete = function () {
        resourceFactory.getService("posts/"+id)
          .delete(function (response) {
            $ionicHistory.goBack();
          });
      }

    }])
