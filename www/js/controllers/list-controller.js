/**
 * Created by AndresFabian on 7/10/2016.
 */
angular.module('starter')
.controller("ListController",["$scope","resourceFactory","$state",function ($scope,resourceFactory,$state) {
  resourceFactory.getService("posts")
    .query(function(response){
      $scope.posts=response;
    });

  $scope.goToNew=function(){
    $state.go("app.new");
  }

  $scope.goToDetail=function(id)
  {
    $state.go("app.detail",{post_id:id});

  }

}])
