/**
 * Created by AndresFabian on 7/10/2016.
 */
angular.module('starter')
  .controller("NewController", ["$scope", "resourceFactory", "$ionicHistory", function ($scope, resourceFactory, $ionicHistory) {
    $scope.post = {};
    $scope.save=function(){
      resourceFactory.getService("posts")
        .save($scope.post, function (response) {
          $ionicHistory.goBack();
        });
    }


  }])
