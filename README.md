---
swagger: "2.0"
info:
  version: "0.0.0"
  title: "CentralPositions"
  contact:
    name: "andres"
  license:
    name: ""
tags:
- name: "Swagger"
- name: "Alarms"
- name: "ConnectionSocket"
- name: "EnterpriseAgents"
- name: "Enterprises"
- name: "EnterpriseVehicles"
- name: "LastPositions"
- name: "VehiclesPosition"
paths:
  /:
    get:
      tags: []
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags: []
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags: []
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags: []
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags: []
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /alarms/disable:
    post:
      tags:
      - "Alarms"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /alarms/disable/{id}:
    get:
      tags:
      - "Alarms"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "Alarms"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "Alarms"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "Alarms"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "Alarms"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /alarms/enable:
    post:
      tags:
      - "Alarms"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /alarms/enable/{id}:
    get:
      tags:
      - "Alarms"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "Alarms"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "Alarms"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "Alarms"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "Alarms"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /authentication:
    get:
      tags: []
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags: []
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags: []
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags: []
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags: []
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /connectionsocket/authentication/{id}:
    get:
      tags:
      - "ConnectionSocket"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "ConnectionSocket"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "ConnectionSocket"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "ConnectionSocket"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "ConnectionSocket"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /connectionsocket/disconnectagent/{id}:
    get:
      tags:
      - "ConnectionSocket"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "ConnectionSocket"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "ConnectionSocket"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "ConnectionSocket"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "ConnectionSocket"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /disconnectAgent:
    post:
      tags:
      - "ConnectionSocket"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /enterpriseagents:
    post:
      tags:
      - "EnterpriseAgents"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /enterpriseagents/create/{id}:
    get:
      tags:
      - "EnterpriseAgents"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "EnterpriseAgents"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "EnterpriseAgents"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "EnterpriseAgents"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "EnterpriseAgents"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /enterprisevehicles/create/{id}:
    get:
      tags:
      - "EnterpriseVehicles"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "EnterpriseVehicles"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "EnterpriseVehicles"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "EnterpriseVehicles"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "EnterpriseVehicles"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/create:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "LastPositions"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/destroy/{id}:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "LastPositions"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/find:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "LastPositions"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/find/{id}:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "LastPositions"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/update/{id}:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "LastPositions"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /lastpositions/{id}:
    get:
      tags:
      - "LastPositions"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "LastPositions"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "LastPositions"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
          schema:
            $ref: "#/definitions/lastpositions"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /swagger/doc:
    get:
      tags:
      - "Swagger"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "Swagger"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "Swagger"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    options:
      tags:
      - "Swagger"
      summary: "Get Resource Options"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /swagger/doc/{id}:
    get:
      tags:
      - "Swagger"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "Swagger"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "Swagger"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /swagger/ui/{id}:
    get:
      tags:
      - "Swagger"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "Swagger"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "Swagger"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "Swagger"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /updateVehiclesPosition:
    get:
      tags:
      - "VehiclesPosition"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
  /vehiclesposition/update/{id}:
    get:
      tags:
      - "VehiclesPosition"
      summary: "Read Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    post:
      tags:
      - "VehiclesPosition"
      summary: "Create Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    put:
      tags:
      - "VehiclesPosition"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    delete:
      tags:
      - "VehiclesPosition"
      summary: "Destroy Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
    patch:
      tags:
      - "VehiclesPosition"
      summary: "Update Object(s)"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "string"
      responses:
        200:
          description: "The requested resource"
        404:
          description: "Resource not found"
        500:
          description: "Internal server error"
definitions:
  contact:
    properties:
      group:
        type: "integer"
        format: "int32"
      id:
        type: "integer"
        format: "int32"
  group:
    properties:
      id:
        type: "integer"
        format: "int32"
  lastpositions:
    properties:
      id:
        type: "integer"
        format: "int32"
      user_id:
        type: "integer"
        format: "int32"
      latitude:
        type: "string"
        format: "string"
      longitude:
        type: "string"
        format: "string"
      address:
        type: "string"
        format: "string"
      accuracy:
        type: "number"
        format: "float"
      provider:
        type: "string"
        format: "string"
      network_status:
        type: "string"
        format: "string"
      operation_id:
        type: "integer"
        format: "int32"
      battery:
        type: "integer"
        format: "int32"
      start_date:
        type: "string"
        format: "date-time"
      end_date:
        type: "string"
        format: "date-time"
  enterprisesagents:
    properties:
      agent_id:
        type: "integer"
        format: "int32"
      enterprise_id:
        type: "integer"
        format: "int32"
      id:
        type: "integer"
        format: "int32"
  enterprisesvehicles:
    properties:
      vehicle_id:
        type: "string"
        format: "string"
      enterprise_id:
        type: "integer"
        format: "int32"
      id:
        type: "integer"
        format: "int32"
  vehiclesgps:
    properties:
      vehicle_id:
        type: "string"
        format: "string"
      gps_device_id:
        type: "string"
        format: "string"
      id:
        type: "integer"
        format: "int32"
  redisenterprisesagents:
    properties:
      agent_id:
        type: "integer"
        format: "int32"
      enterprise_id:
        type: "integer"
        format: "int32"
      id:
        type: "integer"
        format: "int32"
  redisenterprisesvehicles:
    properties:
      vehicle_id:
        type: "string"
        format: "string"
      enterprise_id:
        type: "integer"
        format: "int32"
      id:
        type: "integer"
        format: "int32"
  redisvehiclesgps:
    properties:
      vehicle_id:
        type: "string"
        format: "string"
      gps_device_id:
        type: "string"
        format: "string"
      id:
        type: "integer"
        format: "int32"
  positions:
    properties:
      id:
        type: "integer"
        format: "int32"
      user_id:
        type: "integer"
        format: "int32"
      latitude:
        type: "string"
        format: "string"
      longitude:
        type: "string"
        format: "string"
      address:
        type: "string"
        format: "string"
      accuracy:
        type: "number"
        format: "float"
      provider:
        type: "string"
        format: "string"
      network_status:
        type: "string"
        format: "string"
      battery:
        type: "integer"
        format: "int32"
      start_date:
        type: "string"
        format: "date-time"
      end_date:
        type: "string"
        format: "date-time"
  contact_contacts_contact__group_contacts:
    properties:
      id:
        type: "integer"
        format: "int32"
      group_contacts:
        type: "integer"
        format: "int32"
      contact_contacts_contact:
        type: "integer"
        format: "int32"